SHELL=/bin/bash

#
#  Run four times per-hour and upload if changed
#
07,24,37,54 * * * * root [ -x /usr/sbin/symbiosis-dns-generate ] && /usr/sbin/symbiosis-dns-generate
