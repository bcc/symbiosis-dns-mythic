Symbiosis-dns-mythic
====================

Makes symbiosis compatible with Mythic Beasts DNS API.

There are a few differences from the original, notably that the old
/root/BytemarkDNS directory and upload script is no longer used, with the 
uploads now being performed by symbiosis-dns-mythic. 

Installation
------------
It's best to 'dpkg --purge symbiosis-dns' before installing 
symbiosis-dns-mythic, but at the very least you will have to remove it first
if installed.

Usage
-----
You'll need to configure passwords for your domains on the Mythic Beasts
control panel.

1. Log in to https://ctrlpanel.mythic-beasts.com
2. Click 'my domains'
3. Click on the domain you wish to use.
4. Under 'Nameservers and DNS' click on 'DNS API'.
5. Set a random password. You'll only need to use this once, so make it a
   good one.
6. Go to 'Manage DNS' for your domain, and make sure the template is set to
   'None', or you'll get errors when creating NS records.
7. You'll need to do this for each domain you intend to manage with symbiosis.
   It's best to use a unique password for each domain. The pwgen command is
   excellent for generating them - 'apt-get install pwgen' if it is missing.

Once you've installed the symbiosis-dns-mythic package on your server, you'll
need to give Symbiosis your DNS password. To do this, put it in a file called
'mbpassword' in the /srv/my-brilliant-site.com/config/dns/ directory. 

As root, run 'symbiosis-dns-generate --verbose --force' to regenerate the DNS
records. These will be under
/srv/my-brilliant-site.com/config/dns/my-brillant-site.com.txt as usual, but no
longer in TinyDNS format.

Again, as root, run 'symbiosis-dns-mythic --verbose --force' to force an upload
to the DNS API in verbose mode. This will tell you if you have any template or
auth problems.

These operations will be run automatically from cron in future, after any 
changes.

TODO
----
* Support automatic DNS for subdomains, which symbiosis can't currently do.
* Make --verbose work automatically for uploads too.
* Detect presence of NS records and skip them if the Mythic DNS template is not 'none'
* Make failures a bit more transparent.
* Clean up some of the spurious template warnings.

Packaging
---------
* dch to update changlog.
* debuild -us -uc
* dh_clean to tidy up.
